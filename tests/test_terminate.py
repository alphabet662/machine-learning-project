from board import TicTacToeBoard

class TestWinByRow:
    """Test cases for win by row."""

    def test_row_1(self):
        """Win by the top row."""
        # Position 1,2,3 for X player

        board = TicTacToeBoard()

        board.mark("X",1)
        board.mark("X",2)
        board.mark("X",3)

        assert board.terminate()
        assert board.winner() == "X"

    def test_row_2(self):
        """Win by the middle row."""
        # Position 4,5,6 for X player

        board = TicTacToeBoard()

        board.mark("X",4)
        board.mark("X",5)
        board.mark("X",6)

        assert board.terminate()
        assert board.winner() == "X"

    def test_row_3(self):
        """Win by the bottom row."""
        # Position 7,8,9 for X player

        board = TicTacToeBoard()

        board.mark("X",7)
        board.mark("X",8)
        board.mark("X",9)

        assert board.terminate()
        assert board.winner() == "X"

    def test_row_4(self):
        """Win by the top row."""
        # Position 1,2,3 for O player
        
        board = TicTacToeBoard()

        board.mark("O",1)
        board.mark("O",2)
        board.mark("O",3)

        assert board.terminate()
        assert board.winner() == "O"

    def test_row_5(self):
        """Win by the top row."""
        # Position 1,2,3 for X player
        
        board = TicTacToeBoard()

        board.mark("O",4)
        board.mark("O",5)
        board.mark("O",6)

        assert board.terminate()
        assert board.winner() == "O"

    def test_row_6(self):
        """Win by the top row."""
        # Position 1,2,3 for X player
        
        board = TicTacToeBoard()

        board.mark("O",7)
        board.mark("O",8)
        board.mark("O",9)

        assert board.terminate()
        assert board.winner() == "O"
    
class TestWinByColumn:
    """Test cases for win by column."""

    def test_column_1(self):
        """Win by the left column."""
        # Position 1,4,7 for X player
        
        board = TicTacToeBoard()

        board.mark("X",1)
        board.mark("X",4)
        board.mark("X",7)

        assert board.terminate()
        assert board.winner() == "X"

    def test_column_2(self):
        """Win by the middle column."""
        # Position 2,5,8 for X player

        board = TicTacToeBoard()

        board.mark("X",2)
        board.mark("X",5)
        board.mark("X",8)

        assert board.terminate()
        assert board.winner() == "X"

    def test_column_3(self):
        """Win by the right column."""
        # Position 3,6,9 for X player

        board = TicTacToeBoard()

        board.mark("X",3)
        board.mark("X",6)
        board.mark("X",9)

        assert board.terminate()
        assert board.winner() == "X"

    def test_column_4(self):
        """Win by the left column."""
        # Position 1,4,7 for O player
        
        board = TicTacToeBoard()

        board.mark("O",1)
        board.mark("O",4)
        board.mark("O",7)

        assert board.terminate()
        assert board.winner() == "O"

    def test_column_5(self):
        """Win by the middle column."""
        # Position 2,5,8 for O player
        
        board = TicTacToeBoard()

        board.mark("O",2)
        board.mark("O",5)
        board.mark("O",8)

        assert board.terminate()
        assert board.winner() == "O"

    def test_column_6(self):
        """Win by the right column."""
        # Position 3,6,9 for O player
        
        board = TicTacToeBoard()

        board.mark("O",3)
        board.mark("O",6)
        board.mark("O",9)

        assert board.terminate()
        assert board.winner() == "O"

class TestWinByDiagonal:
    """Test cases for win by diagonal."""

    def test_diagonal_1(self):
        """Win by the diagonal 1."""
        # Position 1,5,9 for X player
        
        board = TicTacToeBoard()

        board.mark("X",1)
        board.mark("X",5)
        board.mark("X",9)

        assert board.terminate()
        assert board.winner() == "X"

    def test_diagonal_2(self):
        """Win by the diagonal 2."""
        # Position 3,5,7 for X player

        board = TicTacToeBoard()

        board.mark("X",3)
        board.mark("X",5)
        board.mark("X",7)

        assert board.terminate()
        assert board.winner() == "X"

    def test_diagonal_3(self):
        """Win by the diagonal 1."""
        # Position 1,5,9 for O player
        
        board = TicTacToeBoard()

        board.mark("O",1)
        board.mark("O",5)
        board.mark("O",9)

        assert board.terminate()
        assert board.winner() == "O"

    def test_diagonal_4(self):
        """Win by the diagonal 2."""
        # Position 3,5,7 for O player

        board = TicTacToeBoard()

        board.mark("O",3)
        board.mark("O",5)
        board.mark("O",7)

        assert board.terminate()
        assert board.winner() == "O"

class TestTieGame:
    
    def test_draw_1(self):
        """Test cases for tie game"""
        board = TicTacToeBoard();
        board.mark("X", 1)
        board.mark("O", 2)
        board.mark("X", 3)
        board.mark("O", 5)
        board.mark("X", 8)
        board.mark("O", 6)
        board.mark("X", 4)
        board.mark("O", 7)
        board.mark("X", 9)
        
        assert board.terminate() == False
        assert board.winner() == None
    
    def test_draw_2(self):
        board = TicTacToeBoard();
        isDraw = False
        board.mark("X", 1)
        board.mark("O", 2)
        board.mark("X", 3)
        board.mark("O", 5)
        board.mark("X", 8)
        board.mark("O", 6)
        board.mark("X", 4)
        board.mark("O", 7)
        board.mark("X", 9)
        
        for i in board.board:
            if i == ".":
                isDraw = False
                break
            else:
                isDraw = True
    
        assert board.terminate() == False
        assert isDraw == True
        assert board.winner() == None