"""The main (startup) function for the application."""

from board import TictactoeBoard

tictactoe = TictactoeBoard()

def inputPlayer():
    tttboard = TictactoeBoard()
    
    while tttboard.playing == True:
        position = input("Please select the position to mark (1-9) : ")
        tttboard.mark(tttboard.symbol,int(position))
        print(tttboard.bucket)

    if tttboard.result == True :
        print(tttboard.symbol + " is winner")
    else:
        print("this game is tie. ")

tictactoe.displayboard()
inputPlayer()