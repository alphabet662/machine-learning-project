"""A representation board for the tic-tac-toe game"""

import random

class TictactoeBoard:
    """The tic-tac-toe board."""

    def __init__(self):
        """Construction."""
        self.result = True
        self.playing = True
        self.board = ["-"]*9
        self.symbol = "X"
        self.bucket = [1,2,3,4,5,6,7,8,9]

    def mark(self, symbol, position):
        """Mark a symbol at a position."""
        self.board[position-1] = symbol
        self.bucket.remove(position)
        return self.winner()

    def displayboard(self):
        print(self.board[0] + " | " + self.board[1] + " | " + self.board[2])
        print(self.board[3] + " | " + self.board[4] + " | " + self.board[5])
        print(self.board[6] + " | " + self.board[7] + " | " + self.board[8])

    def changeSymbol(self):
        if self.symbol == "X":
            self.symbol = "O"
        elif self.symbol == "O":
            self.symbol = "X"
        return self.botPlay(self.symbol)

    def botPlay(self,symbol):
        botChoice = random.choice(self.bucket)
        self.board[botChoice-1] = symbol
        self.bucket.remove(botChoice)
        if self.symbol == "O":
            self.symbol = "X"
        return self.displayboard()

    def winner(self):
        """Get the winner."""
        if self.board[0] == self.board[1] == self.board[2] != '-':
            self.playing = False
            return self.playing
        elif self.board[3] == self.board[4] == self.board[5] != '-':
            self.playing = False
            return self.playing
        elif self.board[6] == self.board[7] == self.board[8] != '-':
            self.playing = False
            return self.playing
        elif self.board[0] == self.board[3] == self.board[6] != '-':
            self.playing = False
            return self.playing
        elif self.board[1] == self.board[4] == self.board[7] != '-':
            self.playing = False
            return self.playing
        elif self.board[2] == self.board[5] == self.board[8] != '-':
            self.playing = False
            return self.playing
        elif self.board[0] == self.board[4] == self.board[8] != '-':
            self.playing = False
            return self.playing
        elif self.board[2] == self.board[4] == self.board[6] != '-':
            self.playing = False
            return self.playing
        elif '-' not in self.board:
            self.playing = False
            self.result = False
            return self.playing

        return self.changeSymbol()

        # rowX_1 = self.board[0] == self.board[1] == self.board[2] != '-'
        # rowX_2 = self.board[3] == self.board[4] == self.board[5] != '-'
        # rowX_3 = self.board[6] == self.board[7] == self.board[8] == '-'
        # columnX_1 = self.board[0] == self.board[3] == self.board[6] != '-'
        # columnX_2 = self.board[1] == self.board[4] == self.board[7] != '-'
        # columnX_3 = self.board[2] == self.board[5] == self.board[8] != '-'
        # diagonalX_1 = self.board[0] == self.board[4] == self.board[8] != '-'
        # diagonalX_2 = self.board[2] == self.board[4] == self.board[6] != '-'

        # # rowO_1 = self.board[0] == self.board[1] == self.board[2] == 'O'
        # # rowO_2 = self.board[3] == self.board[4] == self.board[5] == 'O'
        # # rowO_3 = self.board[6] == self.board[7] == self.board[8] == 'O'
        # # columnO_1 = self.board[0] == self.board[3] == self.board[6] == 'O'
        # # columnO_2 = self.board[1] == self.board[4] == self.board[7] == 'O'
        # # columnO_3 = self.board[2] == self.board[5] == self.board[8] == 'O'
        # # diagonalO_1 = self.board[0] == self.board[4] == self.board[8] == 'O'
        # # diagonalO_2 = self.board[2] == self.board[4] == self.board[6] == 'O'